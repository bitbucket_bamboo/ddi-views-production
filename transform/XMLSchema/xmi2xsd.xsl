<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:uml="http://www.omg.org/spec/UML/20110701" 
	xmlns:xmi="http://www.omg.org/spec/XMI/20110701" xmlns:ddifunc="ddi:functions"
    xmlns:xhtml="http://www.w3.org/1999/xhtml" exclude-result-prefixes="ddifunc uml xmi"
    version="2.0">

    <!-- imports -->
    <xsl:import href="../Util/support.xsl"/>
    <xsl:import href="../Util/markdown.xsl"/>
    
    <!-- options -->
    <xsl:output method="xml" indent="yes"/>
	
	<!-- params -->
	<xsl:param name="filepath" select="'file:///C:/Work/output/xsd/'"/>
	
    <!-- variables -->
    <!--<xsl:variable name="properties" select="document('xsd-properties.xml')"/>-->
	<xsl:variable name="stylesheetVersion">4.2.0</xsl:variable>
	<xsl:variable name="ddiMainVersion">4</xsl:variable>
	<xsl:variable name="ddiMinorVersion"><xsl:value-of select="//packagedElement[@xmi:type='uml:Enumeration' and @name='DDI4Version']/ownedLiteral/@name"/></xsl:variable>
    <xsl:variable name="includedPackages">
        <xsl:for-each select="//packagedElement[@xmi:id='ddi4_model']/packagedElement[@xmi:type='uml:Package']">
            <xsl:text>|</xsl:text>
            <xsl:value-of select="@name"/>
        </xsl:for-each>
        <xsl:text>|</xsl:text>
    </xsl:variable>

    <xsl:template match="xmi:XMI">
        <protocol>
            <xsl:comment>
				<xsl:text>This file was created by xmi2xsd version </xsl:text>
				<xsl:value-of select="$stylesheetVersion"/>
			</xsl:comment>
            <SchemaFiles>
                <xsl:apply-templates select="." mode="library"/>
                <xsl:apply-templates select="//packagedElement[@xmi:id='ddi4_views']/packagedElement[@xmi:type='uml:Package']"
                    mode="view"/>
            </SchemaFiles>
        </protocol>
    </xsl:template>
    
    <xsl:template match="xmi:XMI" mode="library">
        <Library>
            <xsl:attribute name="filename">
                <xsl:value-of select="$filepath"/>
                <xsl:text>/DDI_</xsl:text>
                <xsl:value-of select="$ddiMainVersion"/>
                <xsl:text>-</xsl:text>
                <xsl:value-of select="$ddiMinorVersion"/>
                <xsl:text>.xsd</xsl:text>
            </xsl:attribute>
            <xsl:for-each select="//packagedElement[@xmi:id='ddi4_model']/packagedElement[@xmi:type='uml:Package']">
                <Package>
                    <xsl:attribute name="name" select="@name"></xsl:attribute>
                </Package>
            </xsl:for-each>
        </Library>
        
        <xsl:result-document href="{$filepath}/DDI_{$ddiMainVersion}-{$ddiMinorVersion}.xsd">
            <xs:schema xmlns="urn:ddi.org:4" 
                xmlns:xs="http://www.w3.org/2001/XMLSchema" 
                xmlns:vc="http://www.w3.org/2007/XMLSchema-versioning" 
                targetNamespace="urn:ddi.org:4" 
                elementFormDefault="qualified" 
                attributeFormDefault="unqualified" vc:minVersion="1.1" version="{$ddiMainVersion}-{$ddiMinorVersion}">
                
                <xsl:comment>
        			<xsl:text>This file was created by xmi2xsd version </xsl:text>
        			<xsl:value-of select="$stylesheetVersion"/>
        		</xsl:comment>
                
                <xs:import namespace="http://www.w3.org/XML/1998/namespace" schemaLocation="xml.xsd"/>
                <xs:import namespace="http://www.w3.org/1999/xhtml" schemaLocation="ddi-xhtml11.xsd"/>
                <xs:complexType name="DDIType">
                    <!-- documentation -->
                    <xs:annotation>
                        <xs:documentation>
                            <!--<xsl:value-of select="//*/element[@xmi:idref=$name]/properties/@documentation"/>-->
                        </xs:documentation>
                    </xs:annotation>
                    <xs:sequence minOccurs="1" maxOccurs="1">
                        <xs:element maxOccurs="1" minOccurs="1" ref="DocumentInformation"/>
                        <xs:choice minOccurs="1" maxOccurs="unbounded">
                            <xsl:variable name="viewID" select="@xmi:id"/>
                            <xsl:for-each select="//packagedElement[@xmi:id='ddi4_model']/packagedElement[@xmi:type='uml:Package' and @name!='ComplexDataTypes' and @name!='Primitives']/packagedElement[@xmi:type='uml:Class' and not(@isAbstract='true') and not(@xmi:id='DocumentInformation')]">
                                <xs:element maxOccurs="unbounded" minOccurs="0" >
                                    <xsl:attribute name="ref">
                                        <xsl:value-of select="ddifunc:cleanName(@name)"/>
                                    </xsl:attribute>
                                </xs:element>
                            </xsl:for-each>
                        </xs:choice>
                    </xs:sequence>
                    <xs:attribute name="type"/>
                </xs:complexType>
                <xs:element name="DDI" type="DDIType">
                    <!-- documentation -->
                    <xs:annotation>
                        <xs:documentation>
                            <!--<xsl:value-of select="//*/element[@xmi:idref=$name]/properties/@documentation"/>-->
                        </xs:documentation>
                    </xs:annotation>
                </xs:element>
                <xsl:apply-templates select="//packagedElement[@xmi:id='ddi4_model']/packagedElement[@xmi:type='uml:Package' and @name='ComplexDataTypes']"
                    mode="datatypes"/>
                <xsl:apply-templates select="//packagedElement[@xmi:id='ddi4_model']/packagedElement[@xmi:type='uml:Package' and @name!='ComplexDataTypes' and @name!='Primitives']"
                    mode="package"/>
            </xs:schema>
        </xsl:result-document>
        
    </xsl:template>

    <xsl:template match="packagedElement" mode="view">
        <xsl:variable name="filename">
            <xsl:value-of select="@name"/>
                <xsl:text>_</xsl:text>
                <xsl:value-of select="$ddiMainVersion"/>
                <xsl:text>-</xsl:text>
                <xsl:value-of select="$ddiMinorVersion"/>
                <xsl:text>.xsd</xsl:text>
        </xsl:variable>
        <View>
            <xsl:attribute name="filename">
                <xsl:value-of select="$filepath"/>
                <xsl:text>/</xsl:text>
                <xsl:value-of select="$filename"/>
            </xsl:attribute>
        </View>
        <xsl:result-document href="{$filepath}/{$filename}">
        	<xsl:variable name="name" select="@name"/>
            <xsl:variable name="viewID" select="@xmi:id"/>
            <xs:schema xmlns="urn:ddi.org:4" 
                xmlns:xs="http://www.w3.org/2001/XMLSchema" 
                xmlns:vc="http://www.w3.org/2007/XMLSchema-versioning" 
                targetNamespace="urn:ddi.org:4" 
                elementFormDefault="qualified" 
                attributeFormDefault="unqualified" vc:minVersion="1.1" version="{$ddiMainVersion}-{$ddiMinorVersion}">
                
        		<xsl:comment>
        			<xsl:text>This file was created by xmi2xsd version </xsl:text>
        			<xsl:value-of select="$stylesheetVersion"/>
        		</xsl:comment>
        	    
                <xs:import namespace="http://www.w3.org/XML/1998/namespace" schemaLocation="xml.xsd"/>
                <xs:import namespace="http://www.w3.org/1999/xhtml" schemaLocation="ddi-xhtml11.xsd"/>
                <xs:complexType name="DDIType">
                    <!-- documentation -->
                    <xs:annotation>
                        <xs:documentation>
                            <xsl:value-of select="//*/element[@xmi:idref=$name]/properties/@documentation"/>
                        </xs:documentation>
                    </xs:annotation>
                    <xs:sequence minOccurs="1" maxOccurs="1">
                        <xs:element maxOccurs="1" minOccurs="1" ref="DocumentInformation"/>
                        <xs:choice minOccurs="1" maxOccurs="unbounded">
                            <xsl:apply-templates select="//diagram[model/@package=$viewID]" mode="viewRoot"/>
                        </xs:choice>
                    </xs:sequence>
                    <xs:attribute name="type" use="required">
                    <xs:simpleType>
                        <xs:restriction base="xs:NMTOKEN">
                            <xs:enumeration>
                                <xsl:attribute name="value"><xsl:value-of select="@name"/></xsl:attribute>
                            </xs:enumeration>
                        </xs:restriction>
                    </xs:simpleType>
                    </xs:attribute>
                </xs:complexType>
                <xs:element name="DDI" type="DDIType">
                    <!-- documentation -->
                    <xs:annotation>
                        <xs:documentation>
                            <xsl:value-of select="//*/element[@xmi:idref=$name]/properties/@documentation"/>
                        </xs:documentation>
                    </xs:annotation>
                </xs:element>
                <xsl:apply-templates select="//packagedElement[@xmi:id='ddi4_model']/packagedElement[@xmi:type='uml:Package' and @name='ComplexDataTypes']"
                    mode="datatypes"/>
                <xsl:apply-templates select="//diagram[model/@package=$viewID]" mode="viewClass"/>
                <xsl:apply-templates select="//packagedElement[@xmi:id='ddi4_model']/packagedElement[@xmi:type='uml:Package' and @name!='ComplexDataTypes' and @name!='Primitives']/packagedElement[@xmi:type='uml:Class' and @xmi:id='DocumentInformation']"
                    mode="class"/>
        	</xs:schema>
        </xsl:result-document>
    </xsl:template>

    <xsl:template match="packagedElement" mode="package">
        <xsl:apply-templates select="packagedElement[@xmi:type='uml:Class' and not(@isAbstract='true')]" mode="class"/>
    </xsl:template>
    
    <xsl:template match="packagedElement" mode="datatypes">
        <xsl:apply-templates select="packagedElement[@xmi:type='uml:DataType']" mode="dataType"/>
        <xsl:apply-templates select="//packagedElement[@xmi:type='uml:Enumeration']" mode="enumeration"/>
        <xsl:for-each select="packagedElement[@xmi:type='uml:Class']">
            <xsl:variable name="isSimple">
                <xsl:apply-templates select="." mode="isSimple"/>
            </xsl:variable>
            <xsl:variable name="isXHMTL">
                <xsl:apply-templates select="." mode="isXHMTL"/>
            </xsl:variable>
            <xsl:choose>
                <xsl:when test="$isSimple='true'">
                    <xsl:apply-templates select="." mode="simple"/>
                </xsl:when>
                <xsl:when test="$isXHMTL='true'">
                    <xsl:apply-templates select="." mode="xhtml"/>
                </xsl:when>
                <xsl:when test="@isAbstract='true'">
                    
                </xsl:when>
                <xsl:otherwise>
                    <xsl:apply-templates select="." mode="class"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
        <xs:complexType name="ReferenceType">
            <xs:annotation>
                <xs:documentation>
					Used for referencing an identified entity expressed in DDI by a URN as its node content. 
					
					The lateBound attribute has a boolean value, which - if set to true - indicates that the latest version should be used.
				</xs:documentation>
            </xs:annotation>
            <xs:simpleContent>
                <xs:extension base="xs:anyURI">
                    <xs:attribute name="isExternal" type="xs:boolean" default="false">
                        <xs:annotation>
                            <xs:documentation>Indicates that the reference is made to an external source.</xs:documentation>
                        </xs:annotation>
                    </xs:attribute>
                    <xs:attribute name="lateBound" type="xs:boolean" default="false"/>
                    <xs:attribute name="objectLanguage" type="xs:language" use="optional">
                        <xs:annotation>
                            <xs:documentation>
								Specifies the language (or language-locale pair) to use for display in references to objects which have multiple 
								languages available.
							</xs:documentation>
                        </xs:annotation>
                    </xs:attribute>   
                    <xs:attribute name="sourceContext" type="xs:anyURI" use="optional">
                        <xs:annotation>
                            <xs:documentation>
								An object may be a member of one or more collections providing specific contextual information on membership in the 
								collection and relationship to other members in the collection. If it is important to understand this context, provide 
								the URN of the relevant collection.
							</xs:documentation>
                        </xs:annotation>
                    </xs:attribute>    
                </xs:extension>
            </xs:simpleContent>
        </xs:complexType>
    </xsl:template>
    
    <xsl:template match="diagram" mode="viewRoot">
        <xsl:for-each select="elements/element">
            <xsl:variable name="oID" select="@subject"/>
            <xs:element maxOccurs="unbounded" minOccurs="0" >
                <xsl:attribute name="ref">
                    <xsl:value-of select="ddifunc:cleanName(//packagedElement[@xmi:id=$oID]/@name)"/>
                </xsl:attribute>
            </xs:element>
        </xsl:for-each>
    </xsl:template>
    
    <xsl:template match="diagram" mode="viewClass">
        <xsl:for-each select="elements/element">
            <xsl:variable name="oID" select="@subject"/>
            <xsl:apply-templates select="//packagedElement[@xmi:id='ddi4_model']/packagedElement[@xmi:type='uml:Package' and @name!='ComplexDataTypes' and @name!='Primitives']/packagedElement[@xmi:type='uml:Class' and @xmi:id=$oID]"
                mode="class"/>
        </xsl:for-each>
    </xsl:template>
    
    <xsl:template match="packagedElement" mode="enumeration">
        <xs:simpleType>
            <xsl:attribute name="name">
                <xsl:value-of select="@name"/>
                <xsl:text>Type</xsl:text>
            </xsl:attribute>
            <xs:restriction base="xs:NMTOKEN">
                <xsl:for-each select="ownedLiteral">
                    <xs:enumeration>
                        <xsl:attribute name="value">
                            <xsl:value-of select="@name"/>
                        </xsl:attribute>
                    </xs:enumeration>
                </xsl:for-each>
            </xs:restriction>
        </xs:simpleType>
    </xsl:template>
    
    <xsl:template match="packagedElement" mode="isSimple">
        <xsl:choose>
            <xsl:when test="ownedAttribute[type/@xmi:type!='uml:PrimitiveType' and type/@xmi:type!='xs:language'] or ownedAttribute[not(type/@xmi:type)]">
                <xsl:text>false</xsl:text>
            </xsl:when>
            <xsl:when test="ownedAttribute[(@name='content' and type/@xmi:type='uml:PrimitiveType' and not(contains(type/@href, 'anyURI'))) or type/@xmi:idref='xhtml:BlkNoForm.mix']">
                <xsl:text>true</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>false</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="packagedElement" mode="isXHMTL">
        <xsl:if test="ownedAttribute[@name='content' and type/@xmi:idref='xhtml:BlkNoForm.mix']">
            <xsl:text>true</xsl:text>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="packagedElement" mode="dataType">
        <xs:simpleType>
            <xsl:attribute name="name">
                <xsl:value-of select="@name"/>
                <xsl:text>Type</xsl:text>
            </xsl:attribute>
            <xs:restriction base="xs:string">
                <!-- TODO: currently we dont have the right place to put the pattern in for DataType elements in XMI -->
                <!--
                        <xsl:if test="">
                            <xs:pattern>
                                <xsl:attribute name="value">
                                    <xsl:value-of select=""/>
                                </xsl:attribute>
                            </xs:pattern>
                        </xsl:if>
                        -->
            </xs:restriction>
        </xs:simpleType>
    </xsl:template>
    
    <xsl:template match="packagedElement" mode="xhtml">
        <xsl:variable name="tmpname" select="@name"/>
        <xs:complexType name="StructuredStringType" mixed="true">
            <xsl:attribute name="name">
                <xsl:value-of select="@name"/>
                <xsl:text>Type</xsl:text>
            </xsl:attribute>
            <xs:annotation>
                <xs:documentation>
                    <xsl:value-of select="//*/element[@name=$tmpname]/properties/@documentation"/>
                </xs:documentation>
            </xs:annotation>
            <xs:choice minOccurs="0" maxOccurs="unbounded">
                <xs:group ref="xhtml:BlkNoForm.mix"/>
            </xs:choice>
            <xsl:apply-templates select="ownedAttribute[@name!='content']" mode="attribute"/>
        </xs:complexType>    </xsl:template>
        
    <xsl:template match="packagedElement" mode="simple">
        <xsl:variable name="tmpname" select="@name"/>
        <xsl:choose>
            <xsl:when test="count(ownedAttribute[@xmi:type='uml:Property'])=1 and ownedAttribute[@name='content']">
                <xs:simpleType>
                    <xsl:attribute name="name">
                        <xsl:value-of select="@name"/>
                        <xsl:text>Type</xsl:text>
                    </xsl:attribute>
                    <xs:restriction>
                        <xsl:attribute name="base">
                            <xsl:call-template name="defineType">
                                <xsl:with-param name="xmitype" select="lower-case(tokenize(ownedAttribute[@name='content']/type/@href,'#')[last()])"/>
                            </xsl:call-template>
                        </xsl:attribute>
                        <!-- TODO: currently we dont have the right place to put the pattern in for DataType elements in XMI -->
                        <!--
                        <xsl:if test="">
                            <xs:pattern>
                                <xsl:attribute name="value">
                                    <xsl:value-of select=""/>
                                </xsl:attribute>
                            </xs:pattern>
                        </xsl:if>
                        -->
                    </xs:restriction>
                </xs:simpleType>
            </xsl:when>
            <xsl:otherwise>
                <xs:complexType>
                    <xsl:attribute name="name">
                        <xsl:value-of select="@name"/>
                        <xsl:text>Type</xsl:text>
                    </xsl:attribute>
                    <xs:simpleContent>
                        <xs:extension base="xs:string">
                            <xsl:attribute name="base">
                                <xsl:choose>
                                    <xsl:when test="ownedAttribute[@name='content']">
                                        <xsl:call-template name="defineType">
                                            <xsl:with-param name="xmitype" select="lower-case(tokenize(ownedAttribute[@name='content']/type/@href,'#')[last()])"/>
                                        </xsl:call-template>
                                    </xsl:when>
                                    <xsl:when test="generalization/@general">
                                        <xsl:value-of select="generalization/@general"/>
                                        <xsl:text>Type</xsl:text>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        ERROR
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:attribute>
                            <xsl:apply-templates select="ownedAttribute[@name!='content']" mode="attribute"/>
                        </xs:extension>
                    </xs:simpleContent>
                </xs:complexType>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="packagedElement" mode="class">
        <xsl:variable name="paid">
            <xsl:value-of select="generalization/@general"/>
        </xsl:variable>
        <xsl:variable name="tmpname" select="@name"/>
		<xsl:variable name="mns" select="../@name"/>
        
		<xs:element>
			<xsl:attribute name="name">
				<xsl:value-of select="replace(@name, ':', '_')"/>
			</xsl:attribute>
			<xsl:attribute name="type">
				<xsl:value-of select="@name"/>
				<xsl:text>Type</xsl:text>
			</xsl:attribute>

			<!-- documentation -->
			<xs:annotation>
				<xs:documentation>
					<xsl:value-of select="//*/element[@name=$tmpname]/properties/@documentation"/>
				</xs:documentation>
			</xs:annotation>
		</xs:element>

		<xsl:variable name="gid" select="generalization/@general"/>
		<xsl:variable name="tns" select="//packagedElement[@xmi:id=$gid]/../@name"/>
		
        <xs:complexType>
            <xsl:attribute name="name">
                <xsl:value-of select="ddifunc:cleanName(@name)"/>
                <xsl:text>Type</xsl:text>
            </xsl:attribute>
            <!-- documentation -->
            <xs:annotation>
                <xs:documentation>
                    <xsl:value-of select="//*/element[@name=$tmpname]/properties/@documentation"
                    />
                </xs:documentation>
            </xs:annotation>
            <xs:choice minOccurs="1" maxOccurs="unbounded">
                <xsl:apply-templates select="ownedAttribute[@xmi:type='uml:Property' and not(ends-with(type/@href,'anguage') or ends-with(type/@xmi:type,'anguage') or ends-with(@xmi:type,'anguage'))]"/>
            </xs:choice>
            <xsl:apply-templates select="ownedAttribute[@xmi:type='uml:Property' and (ends-with(type/@href,'anguage') or ends-with(type/@xmi:type,'anguage') or ends-with(@xmi:type,'anguage'))]" mode="attribute"/>
        </xs:complexType>
    </xsl:template>
    
    <xsl:template match="ownedAttribute" mode="attribute">
        <xsl:variable name="name" select="@name"/>
        <xsl:variable name="classname" select="../@name"/>
        <xs:attribute>
                <xsl:choose>
                    <xsl:when test="lower-case(@name)='xmllang' or lower-case(@name)='language'">
                        <xsl:attribute name="ref">
                            <xsl:text>xml:lang</xsl:text>
                        </xsl:attribute>
                        <xsl:attribute name="use">
                            <xsl:choose>
                                <xsl:when test="lowerValue/@value='1'"><xsl:text>required</xsl:text></xsl:when>
                                <xsl:otherwise><xsl:text>optional</xsl:text></xsl:otherwise>
                            </xsl:choose>
                        </xsl:attribute>
                    </xsl:when>
                    <xsl:when test="ends-with(type/@href,'anguage') or ends-with(type/@xmi:type,'anguage') or ends-with(@xmi:type,'anguage')">
                        <xsl:attribute name="name">
                            <xsl:value-of select="@name"/>
                        </xsl:attribute>
                        <xsl:attribute name="type">
                            <xsl:text>xs:language</xsl:text>
                        </xsl:attribute>
                        <xsl:attribute name="use"><xsl:text>optional</xsl:text></xsl:attribute>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:attribute name="name">
                            <xsl:value-of select="@name"/>
                        </xsl:attribute>
                        <xsl:attribute name="type">
                            <!-- define xs type -->
                            <xsl:variable name="xmitype" select="type/@xmi:type"/>
                            <xsl:variable name="xmiidref" select="type/@xmi:idref"/>
                            <xsl:choose>
                                <xsl:when test="lower-case($xmitype) = 'uml:primitivetype'">
                                    <xsl:call-template name="defineType">
                                        <xsl:with-param name="xmitype"
                                            select="lower-case(tokenize(type/@href,'#')[last()])"/>
                                        <xsl:with-param name="package" select="../../@name"/>
                                    </xsl:call-template>
                                </xsl:when>
                                <xsl:when test="$xmitype != ''">
                                    <xsl:call-template name="defineType">
                                        <xsl:with-param name="xmitype" select="$xmitype"/>
                                        <xsl:with-param name="package" select="../../@name"/>
                                    </xsl:call-template>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:call-template name="defineType">
                                        <xsl:with-param name="xmitype" select="$xmiidref"/>
                                        <xsl:with-param name="package" select="../../@name"/>
                                    </xsl:call-template>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:attribute>
                        <xsl:attribute name="use">
                            <xsl:choose>
                                <xsl:when test="lowerValue/@value='1'"><xsl:text>required</xsl:text></xsl:when>
                                <xsl:otherwise><xsl:text>optional</xsl:text></xsl:otherwise>
                            </xsl:choose>
                        </xsl:attribute>
                        <!-- TODO: No example for that in XMI -->
                        <!-- xsl:attribute name="default"><xsl:text>optional</xsl:text></xsl:attribute -->
                    </xsl:otherwise>
                </xsl:choose>
            <!-- documentation -->
            <xs:annotation>
                <xs:documentation>
                    <xsl:value-of select="//*/element[@name=$name]/attributes/attribute[@name=$name]/documentation/@value"/>
                </xs:documentation>
            </xs:annotation>
        </xs:attribute>
    </xsl:template>
        
    <xsl:template match="ownedAttribute">
        <xsl:param name="leftOut"/>
        <xsl:variable name="name" select="@name"/>
        <xsl:variable name="classname" select="../@name"/>
        <xsl:choose>
            <!-- aggregation / association -->
            <xsl:when test="@aggregation or @association">
                <xsl:variable name="paid" select="@association"/>
                <xsl:variable name="nameCheck">
                    <xsl:text>;_</xsl:text>
                    <xsl:value-of select="substring-before(substring-after(@association, '_'), '_')"/>
                    <xsl:text>_;</xsl:text>
                </xsl:variable>
                <xsl:variable name="targetName" select="type/@xmi:idref"/>
                <xsl:if test="not(contains($leftOut, $nameCheck))">
                    <xs:element>
<!--                        <xsl:attribute name="type">
                            <xsl:text>ReferenceType</xsl:text>
                        </xsl:attribute>-->
                        <xsl:for-each select="//packagedElement[@xmi:id=$paid and @xmi:type='uml:Association']">
                            <xsl:attribute name="name">
                                <xsl:value-of select="ddifunc:to-upper-cc(@name)"/>
                            </xsl:attribute>
	                        <xsl:choose>
	                            <xsl:when test="ownedEnd/lowerValue/@value!=''">
	                                <xsl:attribute name="minOccurs">
	                                    <xsl:value-of select="ownedEnd/lowerValue/@value"/>
	                                </xsl:attribute>
	                            </xsl:when>
	                            <xsl:otherwise>
	                                <xsl:attribute name="minOccurs">0</xsl:attribute>
	                            </xsl:otherwise>
	                        </xsl:choose>
	                        <xsl:choose>
	                            <xsl:when test="ownedEnd/upperValue/@value='-1' or ownedEnd/upperValue/@value='' or not(ownedEnd/upperValue/@value)">
	                                <xsl:attribute name="maxOccurs">
	                                    <xsl:text>unbounded</xsl:text>
	                                </xsl:attribute>
	                            </xsl:when>
	                            <xsl:otherwise>
	                                <xsl:attribute name="maxOccurs">
	                                    <xsl:value-of select="ownedEnd/upperValue/@value"/>
	                                </xsl:attribute>
	                            </xsl:otherwise>
	                        </xsl:choose>
                        </xsl:for-each>
                        <!-- documentation -->
                        <xs:annotation>
                            <xs:documentation>
                                <xsl:value-of select="//*/element[@name=$classname]/attributes/attribute[@name=$name]/documentation/@value"/>
                            </xs:documentation>
                        </xs:annotation>
                        <xs:complexType>
                            <xs:complexContent>
                                <xs:extension base="ReferenceType">
                                    <xs:attribute name="typeOfClass" use="required">
                                        <xs:simpleType>
                                            <xs:restriction base="xs:NMTOKEN">
                                                <xsl:if test="not(//packagedElement[@name=$targetName]/@isAbstract='true')">
                                                    <xs:enumeration>
                                                        <xsl:attribute name="value">
                                                            <xsl:value-of select="$targetName"/>
                                                        </xsl:attribute>
                                                    </xs:enumeration>
                                                </xsl:if>
                                                <xsl:apply-templates select="//packagedElement[generalization/@general=$targetName]" mode="fillEnumeration"/>
                                            </xs:restriction>
                                        </xs:simpleType>
                                    </xs:attribute>
                                </xs:extension>
                            </xs:complexContent>
                        </xs:complexType>
                    </xs:element>
                </xsl:if>
            </xsl:when>
            <xsl:otherwise>
                <!-- attribute -->
                <xsl:variable name="nameCheck">
                    <xsl:text>;</xsl:text>
                    <xsl:value-of select="@name"/>
                    <xsl:text>;</xsl:text>
                </xsl:variable>
                <xsl:if test="not(contains($leftOut, $nameCheck))">
                    <xsl:variable name="xmitype" select="type/@xmi:type"/>
                    <xsl:variable name="xmiidref" select="type/@xmi:idref"/>
                    <xsl:choose>
                        <xsl:when test="//packagedElement[@name=$xmiidref]/@isAbstract='true'">
                            <!--
                             <xs:element name="Content" type="TextContentType" minOccurs="0"/> 
                                
                                vs.
                                
                             <xs:choice maxOccurs="unbounded">
                                <xs:element name="LiteralText" type="LiteralTextType"/>
                                <xs:element name="ConditionalText" type="ConditionalTextType"/>
                             </xs:choice>
                            -->
                            <xs:choice>
                                <!-- define min - max -->
                                <xsl:choose>
                                    <xsl:when test="lowerValue/@value!=''">
                                        <xsl:attribute name="minOccurs">
                                            <xsl:value-of select="lowerValue/@value"/>
                                        </xsl:attribute>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:attribute name="minOccurs">0</xsl:attribute>
                                    </xsl:otherwise>
                                </xsl:choose>
                                <xsl:choose>
                                    <xsl:when test="upperValue/@value='-1' or upperValue/@value='' or not(upperValue/@value)">
                                        <xsl:attribute name="maxOccurs">
                                            <xsl:text>unbounded</xsl:text>
                                        </xsl:attribute>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:attribute name="maxOccurs">
                                            <xsl:value-of select="upperValue/@value"/>
                                        </xsl:attribute>
                                    </xsl:otherwise>
                                </xsl:choose>
                                <!-- documentation -->
                                <xs:annotation>
                                    <xs:documentation>
                                        <xsl:value-of select="//*/element[@name=$classname]/attributes/attribute[@name=$name]/documentation/@value"/>
                                    </xs:documentation>
                                </xs:annotation>
                                <xsl:apply-templates select="//packagedElement[@name=$xmiidref]" mode="fillSubstitutions"/>
                            </xs:choice>
                        </xsl:when>
                        <xsl:otherwise>
                            <xs:element>
                                <xsl:attribute name="name">
                                    <xsl:value-of select="ddifunc:to-upper-cc(@name)"/>
                                </xsl:attribute>
                                
                                <!-- define xs type -->
                                <xsl:choose>
                                    <xsl:when test="lower-case($xmitype) = 'uml:primitivetype'">
                                        <xsl:call-template name="defineType">
                                            <xsl:with-param name="xmitype"
                                                select="lower-case(tokenize(type/@href,'#')[last()])"/>
                                            <xsl:with-param name="package" select="../../@name"/>
                                        </xsl:call-template>
                                    </xsl:when>
                                    <xsl:when test="$xmitype != ''">
                                        <xsl:call-template name="defineType">
                                            <xsl:with-param name="xmitype" select="$xmitype"/>
                                            <xsl:with-param name="package" select="../../@name"/>
                                        </xsl:call-template>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:call-template name="defineType">
                                            <xsl:with-param name="xmitype" select="$xmiidref"/>
                                            <xsl:with-param name="package" select="../../@name"/>
                                        </xsl:call-template>
                                    </xsl:otherwise>
                                </xsl:choose>
                                
                                <!-- define min - max -->
                                <xsl:choose>
                                    <xsl:when test="lowerValue/@value!=''">
                                        <xsl:attribute name="minOccurs">
                                            <xsl:value-of select="lowerValue/@value"/>
                                        </xsl:attribute>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:attribute name="minOccurs">0</xsl:attribute>
                                    </xsl:otherwise>
                                </xsl:choose>
                                <xsl:choose>
                                    <xsl:when test="upperValue/@value='-1' or upperValue/@value='' or not(upperValue/@value)">
                                        <xsl:attribute name="maxOccurs">
                                            <xsl:text>unbounded</xsl:text>
                                        </xsl:attribute>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:attribute name="maxOccurs">
                                            <xsl:value-of select="upperValue/@value"/>
                                        </xsl:attribute>
                                    </xsl:otherwise>
                                </xsl:choose>
                                <!-- documentation -->
                                <xs:annotation>
                                    <xs:documentation>
                                        <xsl:value-of select="//*/element[@name=$classname]/attributes/attribute[@name=$name]/documentation/@value"/>
                                    </xs:documentation>
                                </xs:annotation>
                            </xs:element>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:if>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="packagedElement" mode="fillSubstitutions">
        <xsl:variable name="name" select="@name"/>
        <xsl:if test="not(@isAbstract='true')">
            <!-- <xs:element name="LiteralText" type="LiteralTextType"/> -->
            <xs:element>
                <xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute>
                <xsl:attribute name="type">
                    <xsl:value-of select="@name"/>
                    <xsl:text>Type</xsl:text>
                </xsl:attribute>
            </xs:element>
        </xsl:if>
        <xsl:apply-templates select="//packagedElement[generalization/@general=$name]" mode="fillSubstitutions"/>
    </xsl:template>
    
    <xsl:template match="packagedElement" mode="fillEnumeration">
        <xsl:if test="not(@isAbstract='true')">
            <xs:enumeration>
                <xsl:attribute name="value">
                    <xsl:value-of select="@name"/>
                </xsl:attribute>
            </xs:enumeration>
        </xsl:if>
        <xsl:variable name="name" select="@name"/>
        <xsl:apply-templates select="//packagedElement[generalization/@general=$name]" mode="fillEnumeration"/>
    </xsl:template>

    <xsl:template name="defineType">
        <xsl:param name="xmitype"/>
        <xsl:param name="package"></xsl:param>
        
        <xsl:attribute name="type">
            <xsl:choose>
                <!-- string -->
                <xsl:when test="contains($xmitype, 'string')">
                    <xsl:text>xs:string</xsl:text>
                </xsl:when>
                <xsl:when test="contains($xmitype,  'char')">
                    <xsl:text>xs:string</xsl:text>
                </xsl:when>

                <!-- uml unlimitednatural eq string -->
                <xsl:when test="contains($xmitype,  'unlimitednatural')">
                    <xsl:text>xs:string</xsl:text>
                </xsl:when>

                <!-- boolean -->
                <xsl:when test="contains($xmitype,  'boolean')">
                    <xsl:text>xs:boolean</xsl:text>
                </xsl:when>

                <!-- numeric -->
                <xsl:when test="contains($xmitype, 'integer')">
                    <xsl:text>xs:int</xsl:text>
                </xsl:when>
                <xsl:when test="contains($xmitype, 'long')">
                    <xsl:text>xs:long</xsl:text>
                </xsl:when>

                <!-- real -->
                <xsl:when test="contains($xmitype, 'float')">
                    <xsl:text>xs:decimal</xsl:text>
                </xsl:when>
                <xsl:when test="contains($xmitype, 'real')">
                    <xsl:text>xs:decimal</xsl:text>
                </xsl:when>
                
                <!-- date time -->
                <xsl:when test="contains($xmitype, 'datetime')">
                    <xsl:text>xs:dateTime</xsl:text>
                </xsl:when>

                <!-- uri -->
                <xsl:when test="contains($xmitype, 'uri') or contains($xmitype, 'anyURI')">
                    <xsl:text>xs:anyURI</xsl:text>
                </xsl:when>
                
                <!-- language -->
                <xsl:when test="contains($xmitype, 'language')">
                    <xsl:text>xs:language</xsl:text>
                </xsl:when>
                
                <xsl:when test="//packagedElement[@name=$xmitype]">
                    <xsl:value-of select="$xmitype"/>
                    <xsl:text>Type</xsl:text>
                </xsl:when>
                
                <!-- empty and todo types -->
                <xsl:when test="$xmitype =''">
                    <xsl:text>ALERT empty type</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>TODO </xsl:text>
                    <xsl:value-of select="$xmitype"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:attribute>
    </xsl:template>
</xsl:stylesheet>
