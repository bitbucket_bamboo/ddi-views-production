<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:uml="http://www.omg.org/spec/UML/20110701" 
    xmlns:xmi="http://www.omg.org/spec/XMI/20110701" xmlns:ddifunc="ddi:functions"
    xmlns:xhtml="http://www.w3.org/1999/xhtml" exclude-result-prefixes="ddifunc uml xmi"
    version="2.0">
    
    <!-- imports -->
    <xsl:import href="../Util/reStructure.xsl"/>
    <xsl:import href="xmi2dot.xsl"/>
    
    <!-- options -->
    <xsl:output method="text" indent="yes"/>
    
    <!-- params -->
    <xsl:param name="filepath" select="'file:///C:/Work/output/'"/>
    
    <xsl:variable name="stylesheetVersion">5.0.0</xsl:variable>
    <xsl:variable name="ddiMainVersion">4</xsl:variable>
    <xsl:variable name="ddiMinorVersion"><xsl:value-of select="//packagedElement[@xmi:type='uml:Enumeration' and @name='DDI4Version']/ownedLiteral/@name"/></xsl:variable>

    <xsl:template match="xmi:XMI">
        <protocol>
            <xsl:comment>
				<xsl:text>This file was created by xmi2reStructured version </xsl:text>
				<xsl:value-of select="$stylesheetVersion"/>
			</xsl:comment>
            <Packages>
                <xsl:apply-templates select="//packagedElement[@xmi:id='ddi4_model']" mode="index"></xsl:apply-templates>
                <xsl:apply-templates select="//packagedElement[@xmi:id='ddi4_model']/packagedElement[@xmi:type='uml:Package']" mode="package"/>
            </Packages>
            <Views>
                <xsl:apply-templates select="//packagedElement[@xmi:id='ddi4_views']" mode="index"></xsl:apply-templates>
                <xsl:apply-templates select="//packagedElement[@xmi:id='ddi4_views']/packagedElement[@xmi:type='uml:Package']" mode="view"/>
            </Views>
            <Graphs>
                <xsl:apply-templates select="//packagedElement[@xmi:id='ddi4_model']/packagedElement[@xmi:type='uml:Package']" mode="dot">
                    <xsl:with-param name="path" select="$filepath"/>
                </xsl:apply-templates>
                <xsl:apply-templates select="//packagedElement[@xmi:id='ddi4_views']/packagedElement[@xmi:type='uml:Package']" mode="dot">
                    <xsl:with-param name="path" select="$filepath"/>
                </xsl:apply-templates>
                <xsl:apply-templates select="//packagedElement[@xmi:type='uml:Class']" mode="dot">
                    <xsl:with-param name="path" select="$filepath"/>
                </xsl:apply-templates>
            </Graphs>
        </protocol>
    </xsl:template>
    
    <xsl:template match="packagedElement" mode="index">
        <xsl:variable name="indexpath">
            <xsl:value-of select="$filepath"/>
            <xsl:choose>
                <xsl:when test="@xmi:id='ddi4_views'">
                    <xsl:text>/View/</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>/Package/</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:result-document href="{$indexpath}index.rst">
            <xsl:choose>
                <xsl:when test="@xmi:id='ddi4_views'">
					<xsl:text>=====
Views
=====</xsl:text>
                </xsl:when>
                <xsl:otherwise>
					<xsl:text>========
Packages
========</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
			<xsl:text>
			
.. image:: ../../_static/images/ddi-logo.*

.. warning::
  this is a development build, not a final product.

.. toctree::
   :caption: Table of contents
   :maxdepth: 2

</xsl:text>
            <xsl:for-each select="packagedElement[@xmi:type='uml:Package']">
                <xsl:text>   </xsl:text>
                <xsl:value-of select="@name"/>
                <xsl:text>/index.rst</xsl:text>
                <xsl:call-template name="linefeed"/>
            </xsl:for-each>
        </xsl:result-document>
    </xsl:template>
    
    <xsl:template match="packagedElement" mode="package">
        <Package>
            <xsl:attribute name="name" select="@name"/>
        </Package>
        <xsl:result-document href="{$filepath}/Package/{@name}/index.rst">
            <xsl:call-template name="filling">
                <xsl:with-param name="count" select="string-length(@name)"></xsl:with-param>
            </xsl:call-template>
            <xsl:value-of select="@name"/>
            <xsl:call-template name="linefeed"/>
            <xsl:call-template name="filling">
                <xsl:with-param name="count" select="string-length(@name)"></xsl:with-param>
            </xsl:call-template>
            <xsl:apply-templates select="ownedComment/body" mode="reStructure"></xsl:apply-templates>
            <xsl:text>A package is a administrative collection of classes in DDI.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

</xsl:text>
            <xsl:for-each select="packagedElement[@xmi:type='uml:Class' or @xmi:type='uml:Enumeration']">
                <xsl:text>   </xsl:text>
                <xsl:value-of select="@name"/>
                <xsl:text>/index.rst</xsl:text>
                <xsl:call-template name="linefeed"/>
            </xsl:for-each>
            <xsl:text>


Graph
=====

.. graphviz:: /images/graph/</xsl:text>
            <xsl:value-of select="@name"/>
            <xsl:text>/</xsl:text>
            <xsl:value-of select="@name"/>
            <xsl:text>.dot</xsl:text>
        </xsl:result-document>
        <xsl:apply-templates select="packagedElement[@xmi:type='uml:Class' or @xmi:type='uml:Enumeration']" mode="packageClass"/>
    </xsl:template>
    
    <xsl:template match="packagedElement" mode="view">
        <xsl:variable name="viewID" select="@xmi:id"/>
        <View>
            <xsl:attribute name="name" select="@name"/>
        </View>
        <xsl:result-document href="{$filepath}/View/{@name}/index.rst">
            <xsl:call-template name="filling">
                <xsl:with-param name="count" select="string-length(@name)"></xsl:with-param>
            </xsl:call-template>
            <xsl:value-of select="@name"/>
            <xsl:call-template name="linefeed"/>
            <xsl:call-template name="filling">
                <xsl:with-param name="count" select="string-length(@name)"></xsl:with-param>
            </xsl:call-template>
            <xsl:apply-templates select="ownedComment/body" mode="reStructure"></xsl:apply-templates>
            <xsl:text>A functional view is a collection of classes in DDI that covers a functional use case.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

</xsl:text>
            <xsl:for-each select="//diagram[model/@package=$viewID]/elements/element">
                <xsl:text>   </xsl:text>
                <xsl:value-of select="@subject"/>
                <xsl:text>/index.rst</xsl:text>
                <xsl:call-template name="linefeed"/>
            </xsl:for-each>
            <xsl:text>


Graph
=====

.. graphviz:: /images/graph/</xsl:text>
            <xsl:value-of select="@name"/>
            <xsl:text>/</xsl:text>
            <xsl:value-of select="@name"/>
            <xsl:text>.dot</xsl:text>
        </xsl:result-document>
            <xsl:for-each select="//diagram[model/@package=$viewID]/elements/element">
                <xsl:variable name="classId" select="@subject"/>
                <xsl:apply-templates select="//packagedElement[@name=$classId]" mode="packageClass">
                    <xsl:with-param name="forView" select="$viewID"/>
                </xsl:apply-templates>
            </xsl:for-each>
    </xsl:template>
    
    <xsl:template match="packagedElement" mode="packageClass">
        <xsl:param name="forView">none</xsl:param>
        <Class>
            <xsl:attribute name="name" select="@name"/>
        </Class>
        <xsl:variable name="currentPath">
            <xsl:choose>
                <xsl:when test="$forView='none'">
                    <xsl:value-of select="$filepath"/>
                    <xsl:text>/Package/</xsl:text>
                    <xsl:value-of select="../@name"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$filepath"/>
                    <xsl:text>/View/</xsl:text>
                    <xsl:value-of select="$forView"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

		<xsl:variable name="definition">
			<xsl:call-template name="docExtract">
				<xsl:with-param name="partName">||Definition</xsl:with-param>
				<xsl:with-param name="text"><xsl:value-of select="ownedComment/body"/></xsl:with-param>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="examples">
			<xsl:call-template name="docExtract">
				<xsl:with-param name="partName">||Examples</xsl:with-param>
				<xsl:with-param name="text"><xsl:value-of select="ownedComment/body"/></xsl:with-param>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="explain">
			<xsl:call-template name="docExtract">
				<xsl:with-param name="partName">||Explanatory notes</xsl:with-param>
				<xsl:with-param name="text"><xsl:value-of select="ownedComment/body"/></xsl:with-param>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="synonyms">
			<xsl:call-template name="docExtract">
				<xsl:with-param name="partName">||Synonyms</xsl:with-param>
				<xsl:with-param name="text"><xsl:value-of select="ownedComment/body"/></xsl:with-param>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="ddimap">
			<xsl:call-template name="docExtract">
				<xsl:with-param name="partName">||DDI 3.2 mapping</xsl:with-param>
				<xsl:with-param name="text"><xsl:value-of select="ownedComment/body"/></xsl:with-param>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="rdfmap">
			<xsl:call-template name="docExtract">
				<xsl:with-param name="partName">||RDF mapping</xsl:with-param>
				<xsl:with-param name="text"><xsl:value-of select="ownedComment/body"/></xsl:with-param>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="gsimmap">
			<xsl:call-template name="docExtract">
				<xsl:with-param name="partName">||GSIM mapping</xsl:with-param>
				<xsl:with-param name="text"><xsl:value-of select="ownedComment/body"/></xsl:with-param>
			</xsl:call-template>
		</xsl:variable>
		
        <xsl:result-document href="{$currentPath}/{@name}/index.rst">
            <xsl:text>.. _</xsl:text>
            <xsl:value-of select="@name"/>
            <xsl:text>:</xsl:text>
            <xsl:call-template name="linefeed"/>
            <xsl:call-template name="linefeed"/>
            <xsl:call-template name="linefeed"/>
            <xsl:value-of select="@name"/>
            <xsl:call-template name="linefeed"/>
            <xsl:call-template name="filling">
                <xsl:with-param name="count" select="string-length(@name)"></xsl:with-param>
            </xsl:call-template>
            <xsl:if test="generalization">
                <xsl:text>

Extends
=======
:ref:`</xsl:text>
                <xsl:value-of select="generalization/@general"/>
            </xsl:if>
			<xsl:text>


Definition
==========
</xsl:text>
			<xsl:value-of select="$definition"/>
			<xsl:text>


Synonyms
========
</xsl:text>
			<xsl:value-of select="$synonyms"/>
			<xsl:text>


Explanatory notes
=================
</xsl:text>
			<xsl:value-of select="$explain"/>
			<xsl:text>


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst</xsl:text>
		</xsl:result-document>
        <xsl:result-document href="{$currentPath}/{@name}/examples.rst">
            <xsl:text>.. _examples:</xsl:text>
            <xsl:call-template name="linefeed"/>
            <xsl:call-template name="linefeed"/>
            <xsl:call-template name="linefeed"/>
            <xsl:value-of select="@name"/>
			<xsl:text> - Examples</xsl:text>
            <xsl:call-template name="linefeed"/>
            <xsl:call-template name="filling">
                <xsl:with-param name="count" select="string-length(@name) + 11"></xsl:with-param>
            </xsl:call-template>
            <xsl:call-template name="linefeed"/>
            <xsl:call-template name="linefeed"/>
			<xsl:value-of select="$examples"/>
		</xsl:result-document>
        <xsl:result-document href="{$currentPath}/{@name}/ddi32mapping.rst">
            <xsl:text>.. _ddi32mapping:</xsl:text>
            <xsl:call-template name="linefeed"/>
            <xsl:call-template name="linefeed"/>
            <xsl:call-template name="linefeed"/>
            <xsl:value-of select="@name"/>
			<xsl:text> - DDI 3.2 mapping</xsl:text>
            <xsl:call-template name="linefeed"/>
            <xsl:call-template name="filling">
                <xsl:with-param name="count" select="string-length(@name) + 18"></xsl:with-param>
            </xsl:call-template>
            <xsl:call-template name="linefeed"/>
            <xsl:call-template name="linefeed"/>
			<xsl:value-of select="$ddimap"/>
		</xsl:result-document>
        <xsl:result-document href="{$currentPath}/{@name}/rdfmapping.rst">
            <xsl:text>.. _rdfmapping:</xsl:text>
            <xsl:call-template name="linefeed"/>
            <xsl:call-template name="linefeed"/>
            <xsl:call-template name="linefeed"/>
            <xsl:value-of select="@name"/>
			<xsl:text> - RDF mapping</xsl:text>
            <xsl:call-template name="linefeed"/>
            <xsl:call-template name="filling">
                <xsl:with-param name="count" select="string-length(@name) + 14"></xsl:with-param>
            </xsl:call-template>
            <xsl:call-template name="linefeed"/>
            <xsl:call-template name="linefeed"/>
			<xsl:value-of select="$rdfmap"/>
		</xsl:result-document>
        <xsl:result-document href="{$currentPath}/{@name}/gsimmapping.rst">
            <xsl:text>.. _gsimmapping:</xsl:text>
            <xsl:call-template name="linefeed"/>
            <xsl:call-template name="linefeed"/>
            <xsl:call-template name="linefeed"/>
            <xsl:value-of select="@name"/>
			<xsl:text> - GSIM mapping</xsl:text>
            <xsl:call-template name="linefeed"/>
            <xsl:call-template name="filling">
                <xsl:with-param name="count" select="string-length(@name) + 15"></xsl:with-param>
            </xsl:call-template>
            <xsl:call-template name="linefeed"/>
            <xsl:call-template name="linefeed"/>
			<xsl:value-of select="$ddimap"/>
		</xsl:result-document>
        <xsl:result-document href="{$currentPath}/{@name}/fields.rst">
            <xsl:text>.. _fields:</xsl:text>
            <xsl:call-template name="linefeed"/>
            <xsl:call-template name="linefeed"/>
            <xsl:call-template name="linefeed"/>
            <xsl:value-of select="@name"/>
			<xsl:text> - Properties and Relationships</xsl:text>
            <xsl:call-template name="linefeed"/>
            <xsl:call-template name="filling">
                <xsl:with-param name="count" select="string-length(@name) + 31"></xsl:with-param>
            </xsl:call-template>
            <xsl:call-template name="linefeed"/>
            <xsl:call-template name="linefeed"/>
            <xsl:if test="ownedAttribute[not(@association)]">
                <xsl:text>`


Properties
==========

</xsl:text>
                    <xsl:variable name="nameLength">
                    <xsl:for-each select="ownedAttribute[not(@association)]">
                        <xsl:sort select="string-length(@name)" data-type="number" order="descending"/>
                        <xsl:if test="position()=1">
                            <xsl:value-of select="string-length(@name)"/>
                        </xsl:if>
                    </xsl:for-each>
                </xsl:variable>
                <xsl:variable name="idrefLength">
                    <xsl:for-each select="ownedAttribute[not(@association) and type/@xmi:idref]">
                        <xsl:sort select="string-length(type/@xmi:idref)" data-type="number" order="descending"/>
                        <xsl:if test="position()=1">
                            <xsl:value-of select="string-length(type/@xmi:idref)"/>
                        </xsl:if>
                    </xsl:for-each>
                </xsl:variable>
                <xsl:variable name="hrefLength">
                    <xsl:for-each select="ownedAttribute[not(@association) and type/@href]">
                        <xsl:sort select="string-length(type/@href)" data-type="number" order="descending"/>
                        <xsl:if test="position()=1">
	                        <xsl:choose>
	                        	<xsl:when test="contains(type/@href,'#')">
	                        		<xsl:value-of select="string-length(substring-after(type/@href, '#'))"/>
	                        	</xsl:when>
	                        	<xsl:otherwise>
	                        		<xsl:value-of select="string-length(type/@href)"/>
	                        	</xsl:otherwise>
	                        </xsl:choose>
                        </xsl:if>
                    </xsl:for-each>
                </xsl:variable>
                <xsl:variable name="typeLength">
                    <xsl:choose>
						<xsl:when test="$idrefLength = '' and $hrefLength != ''"><xsl:value-of select="$hrefLength"/></xsl:when>
						<xsl:when test="$hrefLength = '' and $idrefLength != ''"><xsl:value-of select="$idrefLength"/></xsl:when>
						<xsl:when test="$hrefLength = '' and $idrefLength = ''">4</xsl:when>
                        <xsl:when test="number($idrefLength) &gt; number($hrefLength)"><xsl:value-of select="$idrefLength"/></xsl:when>
                        <xsl:otherwise><xsl:value-of select="$hrefLength"/></xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:call-template name="filling">
                    <xsl:with-param name="count" select="$nameLength"/>
                    <xsl:with-param name="char">=</xsl:with-param>
                    <xsl:with-param name="linefeed">false</xsl:with-param>
                </xsl:call-template>
                <xsl:text>  </xsl:text>
                <xsl:call-template name="filling">
                    <xsl:with-param name="count" select="$typeLength"/>
                    <xsl:with-param name="char">=</xsl:with-param>
                    <xsl:with-param name="linefeed">false</xsl:with-param>
                </xsl:call-template>
                <xsl:text>  </xsl:text>
                <xsl:call-template name="filling">
                    <xsl:with-param name="count" select="11"/>
                    <xsl:with-param name="char">=</xsl:with-param>
                </xsl:call-template>
                <xsl:text>Name</xsl:text>
                <xsl:call-template name="filling">
                    <xsl:with-param name="count" select="$nameLength - 2"/>
                    <xsl:with-param name="char"><xsl:text> </xsl:text></xsl:with-param>
                    <xsl:with-param name="linefeed">false</xsl:with-param>
                </xsl:call-template>
                <xsl:text>Type</xsl:text>
                <xsl:call-template name="filling">
                    <xsl:with-param name="count" select="$typeLength - 2"/>
                    <xsl:with-param name="char"><xsl:text> </xsl:text></xsl:with-param>
                    <xsl:with-param name="linefeed">false</xsl:with-param>
                </xsl:call-template>
                <xsl:text>Cardinality</xsl:text>
                <xsl:call-template name="linefeed"/>
                <xsl:call-template name="filling">
                    <xsl:with-param name="count" select="$nameLength"/>
                    <xsl:with-param name="char">=</xsl:with-param>
                    <xsl:with-param name="linefeed">false</xsl:with-param>
                </xsl:call-template>
                <xsl:text>  </xsl:text>
                <xsl:call-template name="filling">
                    <xsl:with-param name="count" select="$typeLength"/>
                    <xsl:with-param name="char">=</xsl:with-param>
                    <xsl:with-param name="linefeed">false</xsl:with-param>
                </xsl:call-template>
                <xsl:text>  </xsl:text>
                <xsl:call-template name="filling">
                    <xsl:with-param name="count" select="11"/>
                    <xsl:with-param name="char">=</xsl:with-param>
                </xsl:call-template>
                <xsl:for-each select="ownedAttribute[not(@association)]">
                    <xsl:sort select="@name" data-type="text" order="ascending"/>
                    <xsl:value-of select="@name"/>
                    <xsl:call-template name="filling">
                        <xsl:with-param name="count" select="$nameLength - string-length(@name) + 2"/>
                        <xsl:with-param name="char"><xsl:text> </xsl:text></xsl:with-param>
                        <xsl:with-param name="linefeed">false</xsl:with-param>
                    </xsl:call-template>
                    <xsl:choose>
                        <xsl:when test="type/@xmi:idref">
                            <xsl:value-of select="type/@xmi:idref"/>
                            <xsl:call-template name="filling">
                                <xsl:with-param name="count" select="$typeLength - string-length(type/@xmi:idref) + 2"/>
                                <xsl:with-param name="char"><xsl:text> </xsl:text></xsl:with-param>
                                <xsl:with-param name="linefeed">false</xsl:with-param>
                            </xsl:call-template>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:variable name="aType">
		                        <xsl:choose>
		                        	<xsl:when test="contains(type/@href,'#')">
		                        		<xsl:value-of select="substring-after(type/@href, '#')"/>
		                        	</xsl:when>
		                        	<xsl:otherwise>
		                        		<xsl:value-of select="type/@href"/>
		                        	</xsl:otherwise>
		                        </xsl:choose>
                            </xsl:variable>
                            <xsl:value-of select="$aType"/>
                            <xsl:call-template name="filling">
                                <xsl:with-param name="count" select="$typeLength - string-length($aType) + 2"/>
                                <xsl:with-param name="char"><xsl:text> </xsl:text></xsl:with-param>
                                <xsl:with-param name="linefeed">false</xsl:with-param>
                            </xsl:call-template>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:if test="not(lowerValue/@value)"><xsl:text>0</xsl:text></xsl:if>
                    <xsl:value-of select="lowerValue/@value"/>
                    <xsl:text>..</xsl:text>
                    <xsl:choose>
                        <xsl:when test="not(upperValue/@value)"><xsl:text>n</xsl:text></xsl:when>
                        <xsl:when test="upperValue/@value='-1'"><xsl:text>n</xsl:text></xsl:when>
                        <xsl:otherwise><xsl:value-of select="upperValue/@value"/></xsl:otherwise>
                    </xsl:choose>
                    <xsl:call-template name="linefeed"/>
                </xsl:for-each>
                <xsl:call-template name="filling">
                    <xsl:with-param name="count" select="$nameLength"/>
                    <xsl:with-param name="char">=</xsl:with-param>
                    <xsl:with-param name="linefeed">false</xsl:with-param>
                </xsl:call-template>
                <xsl:text>  </xsl:text>
                <xsl:call-template name="filling">
                    <xsl:with-param name="count" select="$typeLength"/>
                    <xsl:with-param name="char">=</xsl:with-param>
                    <xsl:with-param name="linefeed">false</xsl:with-param>
                </xsl:call-template>
                <xsl:text>  </xsl:text>
                <xsl:call-template name="filling">
                    <xsl:with-param name="count" select="11"/>
                    <xsl:with-param name="char">=</xsl:with-param>
                </xsl:call-template>
                <xsl:call-template name="linefeed"/>
                <xsl:for-each select="ownedAttribute[not(@association)]">
                    <xsl:sort select="@name" data-type="text" order="ascending"/>
                    <xsl:call-template name="linefeed"/>
                    <xsl:value-of select="@name"/>
                    <xsl:call-template name="linefeed"/>
                    <xsl:call-template name="filling">
                        <xsl:with-param name="count" select="string-length(@name)"/>
                        <xsl:with-param name="char">#</xsl:with-param>
                    </xsl:call-template>
                    <xsl:variable name="classname" select="../@name"/>
                    <xsl:variable name="name" select="@name"/>
                    <xsl:value-of select="//element[@name=$classname]/attributes/attribute[@name=$name]/documentation/@value"/>
                    <xsl:call-template name="linefeed"/>
                    <xsl:call-template name="linefeed"/>
                </xsl:for-each>
            </xsl:if>
            <xsl:if test="ownedAttribute[@association]">
                <xsl:text>`


Relationships
=============

</xsl:text>
                <xsl:variable name="nameLength">
                    <!-- xmi:id="Relation_targetObject_source" -->
                    <xsl:for-each select="ownedAttribute[@association]">
                        <xsl:sort select="string-length(@xmi:id)" data-type="number" order="descending"/>
                        <xsl:if test="position()=1">
                            <xsl:value-of select="string-length(@xmi:id) - string-length(../@name) - 8"/>
                        </xsl:if>
                    </xsl:for-each>
                </xsl:variable>
                <xsl:variable name="typeLength">
                    <xsl:for-each select="ownedAttribute[@association]">
                        <xsl:sort select="string-length(type/@xmi:idref)" data-type="number" order="descending"/>
                        <xsl:if test="position()=1">
                            <xsl:value-of select="string-length(type/@xmi:idref)"/>
                        </xsl:if>
                    </xsl:for-each>
                </xsl:variable>
                <xsl:call-template name="filling">
                    <xsl:with-param name="count" select="$nameLength"/>
                    <xsl:with-param name="char">=</xsl:with-param>
                    <xsl:with-param name="linefeed">false</xsl:with-param>
                </xsl:call-template>
                <xsl:text>  </xsl:text>
                <xsl:call-template name="filling">
                    <xsl:with-param name="count" select="$typeLength"/>
                    <xsl:with-param name="char">=</xsl:with-param>
                    <xsl:with-param name="linefeed">false</xsl:with-param>
                </xsl:call-template>
                <xsl:text>  </xsl:text>
                <xsl:call-template name="filling">
                    <xsl:with-param name="count" select="11"/>
                    <xsl:with-param name="char">=</xsl:with-param>
                    <xsl:with-param name="linefeed">
                        <xsl:if test="$forView!='none'">false</xsl:if>
                    </xsl:with-param>
                </xsl:call-template>
                <xsl:if test="$forView!='none'">
                    <xsl:text>  </xsl:text>
                    <xsl:call-template name="filling">
                        <xsl:with-param name="count" select="16"/>
                        <xsl:with-param name="char">=</xsl:with-param>
                    </xsl:call-template>
                </xsl:if>
                <xsl:text>Name</xsl:text>
                <xsl:call-template name="filling">
                    <xsl:with-param name="count" select="$nameLength - 2"/>
                    <xsl:with-param name="char"><xsl:text> </xsl:text></xsl:with-param>
                    <xsl:with-param name="linefeed">false</xsl:with-param>
                </xsl:call-template>
                <xsl:text>Type</xsl:text>
                <xsl:call-template name="filling">
                    <xsl:with-param name="count" select="$typeLength - 2"/>
                    <xsl:with-param name="char"><xsl:text> </xsl:text></xsl:with-param>
                    <xsl:with-param name="linefeed">false</xsl:with-param>
                </xsl:call-template>
                <xsl:text>Cardinality</xsl:text>
                <xsl:if test="$forView!='none'">
                    <xsl:text>  allways external</xsl:text>
                </xsl:if>
                <xsl:call-template name="linefeed"/>
                <xsl:call-template name="filling">
                    <xsl:with-param name="count" select="$nameLength"/>
                    <xsl:with-param name="char">=</xsl:with-param>
                    <xsl:with-param name="linefeed">false</xsl:with-param>
                </xsl:call-template>
                <xsl:text>  </xsl:text>
                <xsl:call-template name="filling">
                    <xsl:with-param name="count" select="$typeLength"/>
                    <xsl:with-param name="char">=</xsl:with-param>
                    <xsl:with-param name="linefeed">false</xsl:with-param>
                </xsl:call-template>
                <xsl:text>  </xsl:text>
                <xsl:call-template name="filling">
                    <xsl:with-param name="count" select="11"/>
                    <xsl:with-param name="char">=</xsl:with-param>
                    <xsl:with-param name="linefeed">
                        <xsl:if test="$forView!='none'">false</xsl:if>
                    </xsl:with-param>
                </xsl:call-template>
                <xsl:if test="$forView!='none'">
                    <xsl:text>  </xsl:text>
                    <xsl:call-template name="filling">
                        <xsl:with-param name="count" select="16"/>
                        <xsl:with-param name="char">=</xsl:with-param>
                    </xsl:call-template>
                </xsl:if>
                <xsl:for-each select="ownedAttribute[@association]">
                    <xsl:sort select="@association" data-type="text" order="ascending"/>
                    <xsl:variable name="associationID" select="@association"/>
                    <xsl:value-of select="//packagedElement[@xmi:id=$associationID]/@name"/>
                    <xsl:call-template name="filling">
                        <xsl:with-param name="count" select="$nameLength - string-length(//packagedElement[@xmi:id=$associationID]/@name) + 2"/>
                        <xsl:with-param name="char"><xsl:text> </xsl:text></xsl:with-param>
                        <xsl:with-param name="linefeed">false</xsl:with-param>
                    </xsl:call-template>
                    <xsl:value-of select="type/@xmi:idref"/>
                    <xsl:call-template name="filling">
                        <xsl:with-param name="count" select="$typeLength - string-length(type/@xmi:idref) + 2"/>
                        <xsl:with-param name="char"><xsl:text> </xsl:text></xsl:with-param>
                        <xsl:with-param name="linefeed">false</xsl:with-param>
                    </xsl:call-template>
                    <xsl:if test="not(lowerValue/@value)"><xsl:text>0</xsl:text></xsl:if>
                    <xsl:value-of select="lowerValue/@value"/>
                    <xsl:text>..</xsl:text>
                    <xsl:choose>
                        <xsl:when test="not(upperValue/@value)"><xsl:text>n</xsl:text></xsl:when>
                        <xsl:when test="upperValue/@value='-1'"><xsl:text>n</xsl:text></xsl:when>
                        <xsl:otherwise><xsl:value-of select="upperValue/@value"/></xsl:otherwise>
                    </xsl:choose>
                    <xsl:if test="$forView!='none'">
                        <xsl:text>           </xsl:text>
                        <xsl:variable name="classId" select="type/@xmi:idref"/>
                        <xsl:apply-templates select="//packagedElement[@xmi:id=$classId]" mode="allwaysExternal">
                            <xsl:with-param name="fromView" select="$forView"/>
                        </xsl:apply-templates>
                    </xsl:if>
                    <xsl:call-template name="linefeed"/>
                </xsl:for-each>
                <xsl:call-template name="filling">
                    <xsl:with-param name="count" select="$nameLength"/>
                    <xsl:with-param name="char">=</xsl:with-param>
                    <xsl:with-param name="linefeed">false</xsl:with-param>
                </xsl:call-template>
                <xsl:text>  </xsl:text>
                <xsl:call-template name="filling">
                    <xsl:with-param name="count" select="$typeLength"/>
                    <xsl:with-param name="char">=</xsl:with-param>
                    <xsl:with-param name="linefeed">false</xsl:with-param>
                </xsl:call-template>
                <xsl:text>  </xsl:text>
                <xsl:call-template name="filling">
                    <xsl:with-param name="count" select="11"/>
                    <xsl:with-param name="char">=</xsl:with-param>
                    <xsl:with-param name="linefeed">
                        <xsl:if test="$forView!='none'">false</xsl:if>
                    </xsl:with-param>
                </xsl:call-template>
                <xsl:if test="$forView!='none'">
                    <xsl:text>  </xsl:text>
                    <xsl:call-template name="filling">
                        <xsl:with-param name="count" select="16"/>
                        <xsl:with-param name="char">=</xsl:with-param>
                    </xsl:call-template>
                </xsl:if>
                <xsl:call-template name="linefeed"/>
                <xsl:for-each select="ownedAttribute[@association]">
                    <xsl:sort select="@association" data-type="text" order="ascending"/>
                    <xsl:variable name="associationID" select="@association"/>
                    <xsl:call-template name="linefeed"/>
                    <xsl:value-of select="//packagedElement[@xmi:id=$associationID]/@name"/>
                    <xsl:call-template name="linefeed"/>
                    <xsl:call-template name="filling">
                        <xsl:with-param name="count" select="string-length(//packagedElement[@xmi:id=$associationID]/@name)"/>
                        <xsl:with-param name="char">#</xsl:with-param>
                    </xsl:call-template>
                    <xsl:apply-templates select="//packagedElement[@xmi:id=$associationID]/ownedComment/body" mode="reStructure"></xsl:apply-templates>
                    <xsl:call-template name="linefeed"/>
                    <xsl:call-template name="linefeed"/>
                </xsl:for-each>
            </xsl:if>
		</xsl:result-document>
        <xsl:result-document href="{$currentPath}/{@name}/graph.rst">
            <xsl:text>.. _graph:</xsl:text>
            <xsl:call-template name="linefeed"/>
            <xsl:call-template name="linefeed"/>
            <xsl:call-template name="linefeed"/>
            <xsl:value-of select="@name"/>
			<xsl:text> - Graph</xsl:text>
            <xsl:call-template name="linefeed"/>
            <xsl:call-template name="filling">
                <xsl:with-param name="count" select="string-length(@name) + 8"></xsl:with-param>
            </xsl:call-template>
            <xsl:call-template name="linefeed"/>
            <xsl:call-template name="linefeed"/>
			<xsl:value-of select="$ddimap"/>
            <xsl:text>


.. graphviz:: /images/graph/</xsl:text>
            <xsl:value-of select="../@name"/>
            <xsl:text>/</xsl:text>
            <xsl:value-of select="@name"/>
            <xsl:text>.dot</xsl:text>
        </xsl:result-document>
    </xsl:template>
    
    <xsl:template match="packagedElement" mode="allwaysExternal">
        <xsl:param name="fromView"/>
        <xsl:variable name="myId" select="@xmi:id"/>
        <xsl:choose>
            <xsl:when test="//diagram[model/@package=$fromView]/elements/element[@subject=$myId]">
                <xsl:text>no</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:variable name="checkSubclasses">
                    <xsl:apply-templates select="//packagedElement[generalization/@general=$myId]" mode="allwaysExternal">
                        <xsl:with-param name="fromView" select="$fromView"></xsl:with-param>
                    </xsl:apply-templates>
                </xsl:variable>
                <xsl:choose>
                    <xsl:when test="contains($checkSubclasses, 'no')">
                        <xsl:text>no</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text>yes</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
</xsl:stylesheet>
